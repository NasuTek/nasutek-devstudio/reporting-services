﻿using System;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;

namespace CrashReporter {
    internal static class Program {
        public static ZipFile ZipFile;
        public static string[] Arguments;

        [STAThread]
        private static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length < 1) {
                var num = (int) MessageBox.Show(
                    "This application is run after a crash to report the problem to the application vendor.  It should not be run directly.",
                    "Crash Reporter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            } else {
                Arguments = args;
                ZipFile = new ZipFile(args[0]);
                Application.Run(new CrashForm());
            }
        }
    }
}
﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace CrashReporter
{
  public partial class UploadReport : Form
  {
    private IContainer components = (IContainer) null;
    private PictureBox pictureBox1;
    private Label label1;
    private ProgressBar progressBar1;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.label1 = new Label();
      this.progressBar1 = new ProgressBar();
      this.pictureBox1 = new PictureBox();
      ((ISupportInitialize) this.pictureBox1).BeginInit();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(9, 75);
      this.label1.Name = "label1";
      this.label1.Size = new Size(241, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Uploading Crash Dump to Crash Reporting Server";
      this.progressBar1.Location = new Point(12, 101);
      this.progressBar1.Name = "progressBar1";
      this.progressBar1.Size = new Size(436, 23);
      this.progressBar1.TabIndex = 2;
      this.pictureBox1.Image = (Image) Properties.Resources.UPload_00;
      this.pictureBox1.Location = new Point(12, 12);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new Size(272, 60);
      this.pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(460, 141);
      this.Controls.Add((Control) this.progressBar1);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.pictureBox1);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "UploadReport";
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Upload Report";
      this.Load += new EventHandler(this.UploadReport_Load);
      ((ISupportInitialize) this.pictureBox1).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
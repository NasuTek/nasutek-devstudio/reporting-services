﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using ICSharpCode.SharpZipLib.Zip;
using NasuTek.API.Aribeth;

namespace CrashReporter {
    public partial class CrashForm : Form {
        public CrashForm() {
            InitializeComponent();
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(Encoding.UTF8.GetString(GetMemStreamForFileInCrashZip("ProductInfo.xml").GetBuffer()));
            label1.Text = string.Format(label1.Text,
                xmlDocument.SelectSingleNode("/ProductInformation/ProductName")?.InnerText);
            foreach (ZipEntry zipEntry in Program.ZipFile)
                treeView1.Nodes[0].Nodes.Add(new TreeNode(zipEntry.Name, 1, 1));
            pi = (ProductInformation) new XmlSerializer(typeof(ProductInformation)).Deserialize(
                GetMemStreamForFileInCrashZip("ProductInfo.xml"));
            if (pi.PrivacyPolicyUri != null)
                return;
            linkLabel1.Visible = false;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e) {
            try {
                LoadXML(Encoding.UTF8.GetString(GetMemStreamForFileInCrashZip(e.Node.Text).GetBuffer()), e.Node.Text);
            } catch {
                treeView2.Nodes.Clear();
                listView1.Items.Clear();
                textBox5.Text = "";
            }
        }

        private void LoadXML(string xml, string docName) {
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xml);
            treeView2.Nodes.Clear();
            treeView2.Nodes.Add(docName);
            IterateNodes(xmlDocument.ChildNodes, treeView2.Nodes[0]);
        }

        private MemoryStream GetMemStreamForFileInCrashZip(string fileName) {
            var memoryStream = new MemoryStream();
            using (var inputStream = Program.ZipFile.GetInputStream(Program.ZipFile.GetEntry(fileName))) {
                var buffer = new byte[2048];
                while (true) {
                    var count = inputStream.Read(buffer, 0, buffer.Length);
                    if (count > 0)
                        memoryStream.Write(buffer, 0, count);
                    else
                        break;
                }
            }

            memoryStream.Position = 0L;
            return memoryStream;
        }

        private static void IterateNodes(XmlNodeList childNodes, TreeNode treeNode) {
            for (var index = 0; index <= childNodes.Count - 1; ++index)
                if (childNodes[index].ToString() == "System.Xml.XmlElement") {
                    var treeNode1 = new TreeNode(childNodes[index].Name) {
                        Tag = childNodes[index]
                    };
                    treeNode.Nodes.Add(treeNode1);
                    if (childNodes[index].ChildNodes.Count > 0)
                        IterateNodes(childNodes[index].ChildNodes, treeNode1);
                }
        }

        private void treeView2_AfterSelect(object sender, TreeViewEventArgs e) {
            if (treeView2.SelectedNode != null && treeView2.SelectedNode.Tag != null &&
                treeView2.SelectedNode.Tag.ToString() == "System.Xml.XmlElement") {
                var tag = (XmlElement) e.Node.Tag;
                textBox5.Text = tag.InnerText;
                listView1.Items.Clear();
                var attributes = tag.Attributes;
                for (var index = 0; index <= attributes.Count - 1; ++index)
                    listView1.Items.Add(attributes[index].Name, 0).SubItems.Add(attributes[index].Value);
            } else {
                textBox5.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e) {
            using (var fileStream = File.Create(Path.Combine(Path.GetTempPath(), "AdditionalInfo.xml"))) {
                new XmlSerializerFactory().CreateSerializer(typeof(AdditionalInfo))
                    ?.Serialize(fileStream,
                        new AdditionalInfo {
                            AdditionalInformation = textBox1.Text,
                            Expected = textBox2.Text,
                            StepsToReproduce = textBox3.Text,
                            WhatWereYouDoing = textBox4.Text
                        });
            }

            Program.ZipFile.BeginUpdate();
            Program.ZipFile.Add(Path.Combine(Path.GetTempPath(), "AdditionalInfo.xml"), "AdditionalInfo.xml");
            Program.ZipFile.CommitUpdate();
            File.Delete(Path.Combine(Path.GetTempPath(), "AdditionalInfo.xml"));

            new UploadReport(pi).ShowDialog();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start(pi.PrivacyPolicyUri);
        }
    }
}
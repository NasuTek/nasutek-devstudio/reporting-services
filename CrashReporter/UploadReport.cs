﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using NasuTek.API.Aribeth;

namespace CrashReporter {
    public partial class UploadReport : Form {
        public UploadReport(ProductInformation productInformation) {
            InitializeComponent();
            var webClient = new WebClient();
            webClient.UploadProgressChanged += webClient_UploadProgressChanged;
            webClient.UploadFileCompleted += webClient_UploadFileCompleted;
            webClient.UploadFileAsync(new Uri(productInformation.CrashReportUri), Program.Arguments[0]);
        }

        private void webClient_UploadFileCompleted(object sender, UploadFileCompletedEventArgs e) {
            var strArray = Encoding.ASCII.GetString(e.Result).Split('|');
            switch (strArray[0]) {
                case "E":
                    var num1 = (int) MessageBox.Show(strArray[1], "Aribeth Crash Reporter", MessageBoxButtons.OK,
                        MessageBoxIcon.Hand);
                    break;
                case "I":
                    var num2 = (int) MessageBox.Show(strArray[1], "Aribeth Crash Reporter", MessageBoxButtons.OK,
                        MessageBoxIcon.Asterisk);
                    Program.ZipFile.Close();
                    File.Delete(Program.Arguments[0]);
                    break;
            }

            Application.Exit();
        }

        private void webClient_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e) {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void UploadReport_Load(object sender, EventArgs e) { }
    }
}
﻿namespace NasuTek.API.Aribeth {
    public class AdditionalInfo {
        public string AdditionalInformation { get; set; }
        public string StepsToReproduce { get; set; }
        public string Expected { get; set; }
        public string WhatWereYouDoing { get; set; }
    }
}
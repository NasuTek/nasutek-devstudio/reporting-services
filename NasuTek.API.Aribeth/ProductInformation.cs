﻿namespace NasuTek.API.Aribeth {
    public class ProductInformation {
        public string ProductName { get; set; }
        public string Version { get; set; }
        public string Build { get; set; }
        public string Branch { get; set; }
        public string CrashReportUri { get; set; }
        public string PrivacyPolicyUri { get; set; }
    }
}
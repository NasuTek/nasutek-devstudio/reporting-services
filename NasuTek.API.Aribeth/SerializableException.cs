﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace NasuTek.API.Aribeth {
    [XmlType(AnonymousType = true, Namespace = "http://nasutek.com/aribeth/crashreporter/exception")]
    [XmlRoot(IsNullable = false, Namespace = "http://nasutek.com/aribeth/crashreporter/exception")]
    [Serializable]
    public class SerializableException {
        public SerializableException() { }

        public SerializableException(Exception exception)
            : this() {
            SetValues(exception);
        }

        public string HelpLink { get; set; } = string.Empty;
        public string Message { get; set; } = string.Empty;
        public string Source { get; set; } = string.Empty;
        public string StackTrace { get; set; } = string.Empty;
        public SerializableException InnerException { get; set; }
        public KeyValuePair<object, object>[] Data { get; set; } = new KeyValuePair<object, object>[0];

        private void SetValues(Exception exception) {
            if (null == exception)
                return;
            HelpLink = exception.HelpLink ?? string.Empty;
            Message = exception.Message ?? string.Empty;
            Source = exception.Source ?? string.Empty;
            StackTrace = exception.StackTrace ?? string.Empty;
            SetData(exception.Data);
            InnerException = new SerializableException(exception.InnerException);
        }

        private void SetData(ICollection collection) {
            Data = new KeyValuePair<object, object>[0];
            collection?.CopyTo(Data, 0);
        }
    }
}
﻿using System;
using System.Xml.Serialization;

namespace NasuTek.API.Aribeth {
    [XmlRoot(IsNullable = false, Namespace = "http://nasutek.com/aribeth/crashreporter/environment")]
    [XmlType(AnonymousType = true, Namespace = "http://nasutek.com/aribeth/crashreporter/environment")]
    [Serializable]
    public class EnvironmentData {
        public int ExitCode => Environment.ExitCode;
        public string CommandLine => Environment.CommandLine;
        public string CurrentDirectory => Environment.CurrentDirectory;
        public string SystemDirectory => Environment.SystemDirectory;
        public string Version => ".NET CLR " + Environment.Version;
        public string OSVersion => Environment.OSVersion.ToString();
        public string StackTrace => Environment.StackTrace;
        public bool Is64BitProcess => Environment.Is64BitProcess;
        public bool Is64BitOperatingSystem => Environment.Is64BitOperatingSystem;
    }
}
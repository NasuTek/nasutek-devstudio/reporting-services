﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using ICSharpCode.SharpZipLib.Zip;

namespace NasuTek.API.Aribeth {
    public class PackExceptDetails {
        public static string Package(Exception e, ProductInformation productInformation) {
            return Package(e, new string[0], productInformation);
        }

        public static string Package(
            Exception e,
            string[] additionalFiles,
            ProductInformation productInformation) {
            var serializerFactory = new XmlSerializerFactory();
            using (var fileStream = File.Create(Path.Combine(Path.GetTempPath(), "AppException.xml"))) {
                serializerFactory.CreateSerializer(typeof(SerializableException))
                    .Serialize(fileStream, new SerializableException(e));
            }

            using (var fileStream = File.Create(Path.Combine(Path.GetTempPath(), "Environment.xml"))) {
                serializerFactory.CreateSerializer(typeof(EnvironmentData))
                    .Serialize(fileStream, new EnvironmentData());
            }

            using (var fileStream = File.Create(Path.Combine(Path.GetTempPath(), "Modules.xml"))) {
                var arrayList = new ArrayList();
                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                    arrayList.Add(assembly.ToString());
                serializerFactory.CreateSerializer(typeof(ArrayList))
                    .Serialize(fileStream, arrayList);
            }

            using (var fileStream = File.Create(Path.Combine(Path.GetTempPath(), "ProductInfo.xml"))) {
                serializerFactory.CreateSerializer(typeof(ProductInformation))
                    .Serialize(fileStream, productInformation);
            }

            var fileName =
                Path.GetInvalidFileNameChars().Aggregate(
                    DateTime.Now.ToString().Replace(' ', '_'),
                    (current, i) => current.Replace(i, '_')) + "-" +
                productInformation.ProductName.Replace(' ', '_') + "-aribeth.zip";
            var zipFile = ZipFile.Create(fileName);
            zipFile.BeginUpdate();
            zipFile.Add(Path.Combine(Path.GetTempPath(), "ProductInfo.xml"), "ProductInfo.xml");
            zipFile.Add(Path.Combine(Path.GetTempPath(), "AppException.xml"), "AppException.xml");
            zipFile.Add(Path.Combine(Path.GetTempPath(), "Environment.xml"), "Environment.xml");
            zipFile.Add(Path.Combine(Path.GetTempPath(), "Modules.xml"), "Modules.xml");
            foreach (var additionalFile in additionalFiles)
                zipFile.Add(additionalFile, Path.GetFileName(additionalFile));
            zipFile.CommitUpdate();
            zipFile.Close();
            File.Delete(Path.Combine(Path.GetTempPath(), "AppException.xml"));
            File.Delete(Path.Combine(Path.GetTempPath(), "Environment.xml"));
            File.Delete(Path.Combine(Path.GetTempPath(), "Modules.xml"));
            File.Delete(Path.Combine(Path.GetTempPath(), "ProductInfo.xml"));
            return fileName;
        }
    }
}